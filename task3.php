<?php
class MyCalculator {
    private $_fval, $_sval;
    public function __construct( $fval, $sval ) {
        $this->_fval = $fval;
        $this->_sval = $sval;
    }
    public function add() {
        return $this->_fval + $this->_sval;
    }
    public function subtract() {
        return $this->_fval - $this->_sval;
    }
    public function multiply() {
        return $this->_fval * $this->_sval;
    }
    public function divide() {
        return $this->_fval / $this->_sval;
    }
}
$mycalc = new MyCalculator(20, 10);
echo $mycalc-> add(); // Displays 18
echo "<br>";
echo $mycalc-> multiply(); // Displays 72
echo "<br>";
echo $mycalc-> subtract(); // Displays 6
echo "<br>";
echo $mycalc-> divide(); // Displays 2
?>