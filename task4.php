<?php
    class factorial_of_a_number{
            protected $_number;

        public function __construct($_number)
        {
           if(!is_int($number)){
               throw new invalidArgumentException('Not a number or missing argument');
           }
        $this->_number = $number;
        }

        public function getNumber()
        {
            return $this->_number;
        }
    }

$newFactorial = new factorial_of_a_number(5);
echo $newFactorial->result();