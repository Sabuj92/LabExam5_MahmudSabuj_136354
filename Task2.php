<?php
$startdate = new DateTime("1971-12-16");
$expecteddate = new DateTime("2016-10-25");

$year = $startdate->diff($expecteddate);
echo "Difference : " . $year->y . " years, " . $year->m." months, ".$year->d." days ";
?>